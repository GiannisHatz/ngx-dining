/* tslint:disable max-line-length */
// tslint:disable: quotemark
export const el = {
  "NewRequestTemplates": {
    "DiningRequestAction": {
      "Name": "Αίτηση Σίτισης",
      "Title": "Αίτηση Σίτισης",
      "Summary": "Αίτηση Σίτισης",
      "Description": "Αιτήσεις Σίτισης",
    },
  },
  "Settings": {
    "EditItem": "Επεξεργασία"
  },
  "StudentStatuses": {
    "active": "Ενεργός",
    "candidate": "Φοιτητής από προεγγραφή",
    "declared": "Ανακηρύχθηκε",
    "erased": "Διαγράφηκε",
    "graduated": "Πτυχιούχος",
    "null": "-",
    "suspended": "Σε αναστολή σπουδών",
  },
  "Requests": {
    "ActionStatusTitle": "Κατάσταση αίτησης",
    "FullName": "Ονοματεπώνυμο",
    "StartTime": "Ημερομηνία αίτησης",
    "StudentIdentifier": "Κωδικός φοιτητή",
    "StudentStatus": "Κατάσταση φοιτητή",
    "RequestedAfter": "Αιτήθηκε μετά από",
    "RequestedBefore": "Αιτήθηκε πριν από",
    "FatherName": "Όνομα Πατρός",
    "MotherName": "Όνομα Μητρός"
  },
  "ActionStatusTypes": {
    "ActiveActionStatus": "Σε εκκρεμότητα",
    "CancelledActionStatus": "Απορρίφθηκε",
    "CompletedActionStatus": "Εγκρίθηκε",
    "FailedActionStatus": "Απέτυχε",
    "PotentialActionStatus": "Προσωρινά αποθηκευμένη",
    "null": "-"
  },
  "DiningCardStatuses": {
    "Active": "Ενεργό",
    "Cancelled": "Άκυρο",
  },
  "Reports": {
    "Viewer": {
      "Close": "Κλείσιμο",
      "Print": "Εκτύπωση αρχείου",
      "Download": "Λήψη αρχείου",
      "Sign": "Ψηφιακή υπογραφή"
    },
  },
  "Students": {
    "GivenName": "ΟΝΟΜΑ",
    "FamilyName": "ΕΠΩΝΥΜΟ",
    "StudentIdentifier": "Κωδικός φοιτητή",
    "FullName": "Ονοματεπώνυμο",
    "StatusTypes": {
      "active": "Ενεργός",
      "erased": "Διαγράφηκε",
      "graduated": "Αποφοίτησε",
      "suspended": "Σε αναστολή",
      "declared": "Ανακηρύχθηκε",
      "candidate": "Από προεγγραφή"
    },
    "StudentTitleSingular": "Ονοματεπώνυμο",
    "DepartmentAbbreviation": "Τμήμα",
    "StudentDepartment": "Τμήμα",
    "StudentCategory": "Κατηγορία φοιτητή"
  },
  "Register": {
    "Accept": "Αποδοχή",
    "Attachments": "Επισυνάψεις",
    "DateCreated": "Δημιουργήθηκε",
    "DateModified": "Τροποποιήθηκε",
    "DateSubmitted": "Υποβλήθηκε",
    "Details": "Λεπτομέρειες",
    "Download": "Λήψη",
    "Message": "Μήνυμα",
    "Messages": "Μηνύματα",
    "NewMessage": "Σύνταξη νέου μηνύματος",
    "NoAttachments": "Δεν υπάρχουν επισυνάψεις",
    "NoMessages": "Δεν υπάρχουν μηνύματα",
    "Preview": "Προεπισκόπηση",
    "Reject": "Απόρριψη",
    "Reset": "Επαναφορά σε προσωρινά αποθηκευμένη",
    "Revert": "Επαναφορά σε εκκρεμότητα",
    "Review": "Αναθεώρηση",
    "SendMessage": "Αποστολή μηνύματος",
    "Status": "ΚΑΤΑΣΤΑΣΗ",
    "UploadFileHelp": "Αποθέστε το αρχείο για επισύναψη ή αναζητήστε το",
    "UploadFilesHelp": "Αποθέστε το αρχείο για επισύναψη ή αναζητήστε το",
    "ComposeNewMessage": {
      "Description": "Η διαδικασία θα προσπαθήσει να στείλει ένα μήνυμα στον υποψήφιο σχετικά με την αίτηση. Γράψτε ένα σύντομο μήνυμα και ξεκινήστε την αποστολή.",
      "Send": "Αποστολή",
      "Cancel": "Άκυρο",
      "Subject": "Θέμα",
      "Title": "Σύνταξη νέου μηνύματος",
      "WriteMessage": "Γράψτε ένα σύντομο μήνυμα",
    },
  },
  "Forms": {
    "DiningRequest": {
      "Disabled": "Α.Μ.Ε.Α",
      "LessThan25yrs": "Έως 25 ετών",
      "LivePermanentlyInSameLocationWithInstitution": "Μόνιμη Διαμονή Θεσσαλονίκη",
      "ForeignScholarStudent": "Αλλοδαπός υπότροφος",
      "Prerequisites": "Πληρεί όλες τις προϋποθέσεις",
      "Unemployment": "Εισπράτει επίδομα ανεργίας",
      "StudentMaritalStatus": "Οικογενειακή Κατάσταση"
    },
    "DiningRequestAction": "Αίτηση Σίτισης",
  },
  "AcademicPeriod": {
    "Summer": "Summer",
    "Winter": "Winter"
  },
  "AcademicYear": "Ακαδημαϊκό έτος", 
  UniversisDiningModule: {
    DiningCardsTemplates: {
      DiningCard: {
        Name: 'Δικαίωμα Σίτισης',
        Title: 'Δικαίωμα Σίτισης',
        Summary: 'Δικαίωμα Σίτισης',
        Description: 'Δικαιώματα Σίτισης',
      },
      validFrom: 'Ισχύει από',
      validThrough: 'Ισχύει έως',
      AcademicPeriod: 'Aκαδημαϊκή περίοδος',
      AcademicYear: "Ακαδημαϊκό έτος", 
      active: 'Κατάσταση δικαιώματος σίτισης',
      CancelItem: "Ακύρωση δικαιώματος",
      CancelItemMessage: {
        title: "Ακύρωση δικαιωμάτων",
        one: "Ένα δικαίωμα ακυρώθηκε με επιτυχία.",
        many: "{{value}} δικαιώματα ακυρώθηκαν με επιτυχία."
      },
      ReactivateItem: "Επανενεργοποίηση δικαιώματος",
      ReactivateMessage: {
        title: "Επανενεργοποίηση δικαιωμάτων",
        one: "Ένα νέο δικαίωμα επανενεργοποιήθηκε με επιτυχία.",
        many: "{{value}} νέα δικαιώματα επανενεργοποιήθηκαν με επιτυχία."
      },
      CancelItemsTitle: "Ακύρωση στοιχείου",
      CancelItemsMessage: "Πρόκειται να ακυρώσετε ένα ή περισσότερα δικαιώματα. Θέλετε να προχωρήσετε?",
    },
    DiningCardStatuses: {
      true: "Ενεργό",
      false: "Άκυρο",
    },
    PersonalInformation: 'Προσωπικά Στοιχεία',
    DiningDocuments: 'Δικαιολογητικά',
    DiningRequestTitle: 'Αίτηση Σίτισης',
    DiningRequestCapitalTitle: 'ΑΙΤΗΣΗ ΣΙΤΙΣΗΣ',
    DocumentsSubmission: {
      Title: 'Κατάθεση εγγράφων',
      Subtitle: 'Ακολούθησε τις παρακάτω οδηγίες για να καταθέσεις τα απαιτούμενα έγγραφα για την αίτηση σίτισής σου.',
      SubmissionStatus: 'Κατάσταση κατάθεσης',
      SubmissionStatuses: {
        pending: 'Η κατάθεση εγγράφων για την σίτιση είναι σε εξέλιξη',
        completed: 'Η κατάθεση εγγράφων για την σίτιση έχει ολοκληρωθεί',
        failed: 'Η κατάθεση εγγράφων για την σίτιση είναι σε εξέλιξη',
        unavailable: 'Η κατάθεση εγγράφων δεν είναι διαθέσιμη'
      },
      AttachmentDeleteModal: {
        Title: 'Διαγραφή εγγράφου',
        Body: 'Διαγραφή εγγράφου τύπου {{attachmentType}};',
        Notice: 'Αυτή η πράξη δεν είναι αναιρέσιμη.',
        Close: 'Κλείσιμο',
        Delete: 'Διαγραφή'
      },
      DiningDocumentToUpload: 'έγγραφο για μεταφόρτωση',
      DiningDocumentsToUpload: 'έγγραφα για μεταφόρτωση',
      DiningDocumentsPhysicals: 'Έγγραφα σίτισης που πρέπει να παραδοθούν στην γραμματεία του τμήματος',
      DownloadDocument: 'Λήψη εγγράφου',
      UploadDocument: 'Μεταφόρτωση',
      RemoveDocument: 'Αφαίρεση αρχείου',
      ContactService: 'Επικοινωνία με υπεύθυνο',
      Errors: {
        Download: 'Υπήρξε σφάλμα κατά την λήψη του αρχείου',
        Remove: 'Υπήρξε σφάλμα κατά την αφαίρεση του αρχείου',
        Upload: 'Υπήρξε σφάλμα κατά την μεταφόρτωση του αρχείου'
      }
    },
    ModalConfirm: {
      Submit: 'Ολοκλήρωση',
      Close: 'Κλείσιμο',
      Title: 'Αποστολή Αίτησης Σίτισης',
      Body: 'Θέλετε να στείλετε την αίτηση σας για αποστολή και έλεγχο στην γραμματεία;'
    },
    Messages: {
      Title: 'Μηνύματα',
      NewMessage: 'Νέο μήνυμα',
      Subject: 'Θέμα',
      WriteMessage: 'Το μήνυμα σας',
      NoSubject: 'Χωρίς θέμα',
      IncomingMessage: 'Εισερχόμενο μήνυμα',
      NoMessages: 'Κανένα μήνυμα'
    },
    MessagePrompt: 'Το μήνυμά σου',
    Send: 'Αποστολή',
    Cancel: 'Ακύρωση',
    Date: 'Ημερομηνία',
    Time: 'Ώρα',
    Location: 'Τοποθεσία',
    Download: 'Λήψη',
    Previous: 'Προηγούμενο',
    Next: 'Επόμενο',
    Submit: 'Υποβολή',
    Completed: 'Ολοκλήρωση Αίτησης',
    ContactRegistrar: 'Επικοινωνία με Γραμματεία',
    StudentInfo: 'Στοιχεία Φοιτητή',
    StudyGuide: 'ΟΔΗΓΟΣ ΣΠΟΥΔΩΝ',
    Specialty: 'ΚΑΤΕΥΘΥΝΣΗ',
    Prerequisites: 'Προϋποθέσεις',
    Progress: 'Πρόοδος',
    NoRulesFound: 'Οι προϋποθέσεις πτυχίου δεν έχουν οριστεί.',
    CourseType: 'Τύπος Μαθημάτων',
    AllTypeCourses: 'Όλοι οι τύποι μαθημάτων',
    Thesis: 'Εργασία',
    Student: 'Ιδιότητες Φοιτητή',
    Internship: 'Πρακτική',
    Course: 'Προαπαιτούμενο Μάθημα',
    CourseArea: 'Γνωστικό Αντικείμενο',
    CourseCategory: 'Κατηγορία Μαθήματος',
    CourseSector: 'Τομέας Μαθημάτων',
    ProgramGroup: 'Ομάδα Μαθημάτων',
    StatusLabel: 'Κατάσταση της αίτησης σου',
    NoAttachments: 'Δεν υπαρχουν έγγραφα για μεταφόρτωση',
    TemporarySaveMessage: 'Η αίτησή σας αποθηκεύτηκε προσωρινά',
    AttachDocumentMessage: 'Επισυνάψτε τα δικαιολογητικά που απαιτούνται',
    RequestPeriodExpired: 'H περίοδος αιτήσεων σίτισης έχει λήξει.',
    RequestPeriodNotStarted: 'Η περίοδος αιτήσεων σίτισης θα είναι ανοιχτή από {{dateStart}} εώς {{dateEnd}}.',
    NoDiningRequestEvent: 'Η περίοδος αιτήσεων σίτισης δεν έχει οριστεί.',
    EmptyDocumentList: "Η λίστα των απαραίτητων εγγράφων είναι κενή.",
    EmptyDocumentListContinue: "Η λίστα των απαραίτητων εγγράφων είναι κενή. Μπορείτε να συνεχίσετε με την υποβολή της αίτησης.",
    "AcceptConfirm": {
      "Title": "Αποδοχή και ολοκλήρωση",
      "Message": "Πρόκειται να αποδεχτείτε την αίτηση. Μετά από αυτό ο υποψήφιος θα αποκτήσει μία νέα κάρτα σίτισης. Θέλετε να συνεχίσετε;"
    },
    "RejectConfirm": {
      "Title": "Απόρριψη αίτησης",
      "Message": "Πρόκειται να απορρίψετε την αίτηση. Ο υποψήφιος θα ενημερωθεί σχετικά με την απόρριψη. Θέλετε να συνεχίσετε;"
    },
    "ResetConfirm": {
      "Title": "Αλλαγή κατάστασης αίτησης",
      "Message": "Πρόκειται να θέσετε την κατάσταση της αίτησης 'Σε εκκρεμότητα'. Μετά από αυτό ο υποψήφιος θα μπορεί να κάνει αλλαγές στην αίτηση και να την αποστείλει εκ νέου. Θέλετε να συνεχίσετε;"
    },
    "RevertConfirm": {
      "Title": "Επανεργοποίηση αίτησης",
      "Message": "Πρόκειται να ενεργοποιήσετε ξανά την αίτηση. Θέλετε να συνεχίσετε;"
    },
    CancelCardModal:{
      "ResetConfirm": {
        "Title": "Αλλαγή κατάστασης του δικαιώματος σίτισης",
        "Message": "Πρόκειται να θέσετε την κατάσταση του δικαιώματος 'Άκυρο'. Θέλετε να συνεχίσετε;"
      },
      Title: "Ακύρωση δικαιώματος σίτισης",
      Help: "Δηλώστε τον λόγο ακύρωσης του δικαιώματος σίτισης. Ο/Η δικαιούχος θα ενημερωθεί μέσω μηνύματος.",
      CancelNamePlaceholder: "Εισάγετε τον λόγο ακύρωσης",
      Accept: "Ακύρωση",
      Close: "Κλείσιμο",
      TitleOnUserError: "Πρέπει να δηλώσετε τον λόγο ακύρωσης."
    },
    CancelCardSubject: "Ακύρωση δικαιώματος σίτισης",
    CancelCardBody: "Το δικαίωμα σίτισης ακυρώθηκε λόγω: {{cancelReason}}",
    RevertCard: "Επαναφορά",
    RejectAttachmentModal: {
      Title: "Απόρριψη δικαιολογητικού",
      Help: "Δηλώστε τον λόγο απόρριψης του δικαιολογητικού. Ο/Η αιτών/αιτούσα θα ενημερωθεί μέσω μηνύματος.",
      RejectionNamePlaceholder: "Εισάγετε τον λόγο απόρριψης",
      Accept: "Απόρριψη",
      Close: "Κλείσιμο",
      TitleOnUserError: "Πρέπει να δηλώσετε τον λόγο απόρριψης."
    },
    RejectAttachmentSubject: "Απόρριψη δικαιολογητικού αίτησης σίτισης",
    RejectAttachmentBody: "Το δικαιολογητικό: '{{attachmentName}}' της αίτησης σίτισης απορρίφθηκε λόγω: {{rejectionName}}",
    RevertAttachment: "Επαναφορά",
    DiningClubAttachmentNote: 'Η Λέσχη ή η Επιτροπή Φοιτητικής Μέριμνας του Ιδρύματος, εάν σε αυτό δεν λειτουργεί Λέσχη, μπορεί να ζητά και άλλα, κατά την κρίση της αποδεικτικά στοιχεία για την οικονομική και περιουσιακή κατάσταση του ενδιαφερόμενου, προκειμένου να αποφανθεί αν δικαιούται ή όχι σίτισης.',
  }

};
