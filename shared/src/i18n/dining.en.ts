/* tslint:disable max-line-length */
// tslint:disable: quotemark
export const en = {
  NewRequestTemplates: {
    DiningRequestAction: {
      "Name": "Dining Request",
      "Title": "Dining Request",
      "Summary": "Dining Request",
      "Description": "Dining Requests"
    }
  },
  "Settings": {
    "EditItem": "Edit"
  },
  "StudentStatuses": {
    "active": "Active",
    "candidate": "Candidate",
    "declared": "Declared",
    "erased": "Erased",
    "graduated": "Graduated",
    "null": "-",
    "suspended": "Suspended",
  },
  "Requests": {
    "ActionStatusTitle": "Request Status",
    "FullName": "Full Name",
    "StartTime": "Request date",
    "StudentIdentifier": "Student Identifier",
    "StudentStatus": "Student Status",
    "RequestedAfter": "Requested after",
    "RequestedBefore": "Requested before",
    "FatherName": "Father's Name",
    "MotherName": "Mother's Name",
  },
  "ActionStatusTypes": {
    "ActiveActionStatus": "Active",
    "CancelledActionStatus": "Cancelled",
    "CompletedActionStatus": "Completed",
    "FailedActionStatus": "Failed",
    "PotentialActionStatus": "Potential",
    "null": "-"
  },
  "DiningCardStatuses": {
    "Active": "Active",
    "Cancelled": "Cancelled",
  },
  "Reports": {
    "Viewer": {
      "Close": "Close",
      "Download": "Download file",
      "Print": "Print file",
      "Sign": "Sign document"
    },
  },
  "Students": {
    "FamilyName": "FAMILY NAME",
    "GivenName": "GIVEN NAME",
    "StudentIdentifier": "Student Identifier",
    "FullName": "Full Name",
    "StatusTypes": {
      "active": "Active",
      "candidate": "Candidate",
      "declared": "Graduated",
      "erased": "Erased",
      "graduated": "Graduated",
      "suspended": "Suspended"
    },
    "DepartmentAbbreviation": "Department name",
    "StudentDepartment": "Department name",
    "StudentCategory": "Student category"
  },
  "Register": {
    "Accept": "Accept",
    "Attachments": "Attachments",
    "DateCreated": "Created at",
    "DateModified": "Modified at",
    "DateSubmitted": "Submitted at",
    "Details": "Details",
    "Download": "Download",
    "Message": "Message",
    "Messages": "Messages",
    "NewMessage": "Compose new message",
    "NoAttachments": "No attachments yet",
    "NoMessages": "No messages yet",
    "Preview": "Preview",
    "Reject": "Reject",
    "Reset": "Reset application to pending",
    "Revert": "Revert to active",
    "Review": "Review",
    "SendMessage": "Send a message",
    "Status": "Status",
    "UploadFileHelp": "Drop file to attach, or browse",
    "UploadFilesHelp": "Drop files to attach, or browse",
    "ComposeNewMessage": {
      "Title": "Compose new message",
      "Description": "The operation will try to send a message to the candidates of the selected applications. Please write a short message below and start sending messages.",
      "Subject": "Subject",
      "WriteMessage": "Write a short message",
      "Send": "Send",
      "Cancel": "Cancel",
    },
  },
  "Forms": {
    "DiningRequest": {
      "Disabled": "Disabled",
      "LessThan25yrs": "Less than 25yrs old",
      "LivePermanentlyInSameLocationWithInstitution": "Student Lives Permanently In Same Location With Institution",
      "ForeignScholarStudent": "Foreign Scholar Student",
      "Prerequisites": "Prerequisites",
      "Unemployment": "Student Receives Unemployment Benefit",
      "StudentMaritalStatus": "Marital Status"
    },
    "DiningRequestAction": "Dining Request"
  },
  "AcademicPeriod": {
    "summer": "Summer",
    "winter": "Winter"
  },
  UniversisDiningModule: {
    DiningCardsTemplates: {
      DiningCard: {
        Name: 'Dining Card',
        Title: 'Dining Card',
        Summary: 'Dining Card',
        Description: 'Dining Cards',
      },
      validFrom: 'Valid From',
      validThrough: 'Valid Through',
      active: 'Dining Card Status',
      CancelItem: "Cancellation of dining card",
      ReactivateItem: "Reactivation of dining card",
      AcademicYear: "Academic Year",
      AcademicPeriod: "Academic Period",
    },
    DiningCardStatuses: {
      true: "Active",
      false: "Suspended",
    },
    PersonalInformation: 'Personal Information',
    DiningDocuments: 'Documents',
    DiningRequestTitle: 'Dining request',
    DiningRequestCapitalTitle: 'DINING REQUEST',
    DocumentsSubmission: {
      Title: ' Documents submission',
      Subtitle: ' Follow the instructions in order to submit the required documents for your dining request.',
      SubmissionStatus: 'Submission status',
      SubmissionStatuses: {
        pending: 'Document submission for dining is ongoing',
        completed: 'Document submission for dining is completed',
        failed: 'Document submission for dining is ongoing',
        unavailable: 'Document submission is not available'
      },
      AttachmentDeleteModal: {
        Title: 'Document delete',
        Body: 'Delete document of {{attachmentType}} type?',
        Notice: 'This action can not be undone.',
        Close: 'Close',
        Delete: 'Delete'
      },
      DiningDocumentToUpload: 'document for upload',
      DiningDocumentsToUpload: 'documents for upload',
      DiningDocumentsPhysicals: 'documents to be delivered to the secretariat of the department',
      DownloadDocument: 'Download document',
      UploadDocument: 'Upload document',
      RemoveDocument: 'Remove document',
      ContactService: 'contact related service',
      Errors: {
        Download: 'There was an error during the file download',
        Remove: 'There was an error during the file removal',
        Upload: 'There was an error during the file upload'
      }
    },
    ModalConfirm: {
      Submit: 'Submit',
      Close: 'Close',
      Title: 'Send Dining Request',
      Body: 'You want to send your request for sending and check to the secretariat?'
    },
    Messages: {
      Title: 'Messages',
      NewMessage: 'New message',
      NoSubject: 'No subject',
      Subject: 'Subject',
      WriteMessage: 'Your message',
      IncomingMessage: 'Incoming message',
      NoMessages: 'No messages'
    },
    MessagePrompt: 'Your message',
    Send: 'Send',
    Cancel: 'Cancel',
    Date: 'Date',
    Time: 'Time',
    Location: 'Location',
    Download: 'Download',
    Previous: 'Previous',
    Next: 'Next',
    Submit: 'Submit',
    Completed: 'Request Completion',
    ContactRegistrar: 'Contact Registrar',
    StudentInfo: 'Student Information',
    StudyGuide: 'STUDY GUIDE',
    Specialty: 'SPECIALTY',
    Prerequisites: 'Prerequisites',
    Progress: 'Progress',
    NoRulesFound: 'Dining Rules have not been set.',
    CourseType: 'Course Type',
    AllTypeCourses: 'All Type Courses',
    Thesis: 'Thesis',
    Student: 'Semester',
    Internship: 'Internship',
    Course: 'Prerequisite Courses',
    CourseArea: 'Courses Area',
    CourseCategory: 'Courses Category',
    CourseSector: 'Courses Sector',
    ProgramGroup: 'Courses Group',
    StatusLabel: 'Status of your request',
    NoAttachments: 'There are no attachments for upload',
    TemporarySaveMessage: 'Dining request has been saved temporarily',
    AttachDocumentMessage: 'Please attach all required documents',
    RequestPeriodExpired: 'The period for dining requests has expired.',
    RequestPeriodNotStarted: 'Dining request period will be open from {{dateStart}} to {{dateEnd}}.',
    NoDiningRequestEvent: 'No dining request period defined.',
    EmptyDocumentList: "The list of required documents is empty.",
    EmptyDocumentListContinue: "The list of required documents is empty. You can continue by submitting your application.",
    "AcceptConfirm": {
      "Title": "Accept and complete",
      "Message": "You are going to accept this application. After this operation the candidate will have a new student dining card. Do you want to proceed?"
    },
    "RejectConfirm": {
      "Title": "Reject application",
      "Message": "You are going to reject this application. The candidate will be informed about application rejection. Do you want to proceed?"
    },
    "ResetConfirm": {
      "Title": "Change application status",
      "Message": "You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?"
    },
    "RevertConfirm": {
      "Title": "Activate application",
      "Message": "You are going to activate this application again. Do you want to proceed?"
    },
    CancelCardModal: {
      "ResetConfirm": {
        "Title": "Αλλαγή κατάστασης του δικαιώματος σίτισης",
        "Message": "Πρόκειται να θέσετε την κατάσταση του δικαιώματος 'Άκυρο'. Θέλετε να συνεχίσετε;"
      },
      Title: "Cancellation of dining card",
      Help: "Specify the reason of cancellation. The candidate will be notified by message.",
      CancelNamePlaceholder: "Enter cancellation reason",
      Accept: "Cancellation of Dining Card",
      Close: "Close",
      TitleOnUserError: "You must specify the reason of cancellation."
    },
    CancelCardSubject: "Cancellation of dining card",
    CancelCardBody: "The dining card was cancelled due: {{cancelReason}}",
    RevertCard: "Revert",
    RejectAttachmentModal: {
      Title: "Reject document",
      Help: "Specify the reason of rejection. The candidate will be notified by message.",
      RejectionNamePlaceholder: "Enter rejection reason",
      Accept: "Reject",
      Close: "Close",
      TitleOnUserError: "You must specify the reason of rejection."
    },
    RejectAttachmentSubject: "Rejection of dining request document",
    RejectAttachmentBody: "Document: '{{attachmentName}}' of your dining request was reject with reason: {{rejectionName}}",
    RevertAttachment: "Revert",
    DiningClubAttachmentNote: 'The Dining club or Student Welfare Committee Foundation, if it does not have a club, can request other evidence at its discretion for the financial and property situation of the individual concerned, to decide whether he is entitled to the free dining right'
  }

};
