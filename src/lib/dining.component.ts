import { Component, OnInit, OnDestroy, Input, ViewChild, AfterViewInit,
  EventEmitter, Output, OnChanges, SimpleChanges, ChangeDetectorRef,
  SimpleChange, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedUser, LoadingService, ErrorService, UserService, ModalService, DIALOG_BUTTONS } from '@universis/common';
import { Subscription, forkJoin, combineLatest } from 'rxjs';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { AdvancedFormComponent } from '@universis/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep, template } from 'lodash';
import * as moment_ from 'moment';
const moment = moment_;

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-dining',
  templateUrl: './dining.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./dining.component.scss']
})
export class DiningComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  constructor(private _context: AngularDataContext,
    private _userService: UserService,
    private _loading: LoadingService,
    private _errorService: ErrorService,
    private _activatedRoute: ActivatedRoute,
    private _http: HttpClient,
    private _modal: ModalService,
    private _translateService: TranslateService,
    private _router: Router,
    private cd: ChangeDetectorRef) { }

  /**
   * Returns the current model state (for insert or update)
   */
  get modelState(): string {
    if (this.model && typeof this.model.id === 'number') {
      return 'update';
    }
    return 'insert';
  }

  get documentState(): string {
    if (this.documents == null) {
      return 'invalid';
    }
    const required = this.documents.filter((item) => {
      return item.numberOfAttachments > 0 && item.attachments.length < item.numberOfAttachments;
    }).length;
    if (required > 0) {
      return 'invalid';
    }
    return 'valid';
  }

  queryParamSubscription: Subscription;

  public showNewMessage = false;
  public newMessage: { subject?: string; body?: string; attachments: any[]; } = {
    attachments: []
  };
  public messages: any[];
  public nextButtonDisabled = true;
  public documentStateChange: EventEmitter<any> = new EventEmitter<any>();
  public documents: { attachments: any[]; attachmentType: any; numberOfAttachments: number }[];

  public formAction: string;
  public askMoreAttachmentsForIncomeSiblings: boolean = false;
  public moreAttachmentNumberForIncomeSiblings: any;
  public askMoreAttachmentsForIncomeParents: boolean = false;
  public moreAttachmentNumberForIncomeParents: any;
  public askMoreAttachmentsForIncomeStudentChildren: boolean = false;
  public moreAttachmentNumberStudentChildren: any;
  public student: any;
  public loading = true;

  @Input() model: any = {
    attachments: [],
    actionStatus: {
      alternateName: 'PotentialActionStatus'
    },
    effectiveStatus: {
      alternateName: 'AcceptedAttachmentsEffectiveStatus'
    }
  };
  @Input() diningRequestEvent: any;
  @Input() studentDiningCard: boolean = false;
  @ViewChild('tabset', {
  }) tabset: TabsetComponent;
  @ViewChild('form1', {
  }) form1: AdvancedFormComponent;
  @ViewChild('attachDocuments', {
  }) attachDocuments: TabDirective;

  ngOnChanges(changes: SimpleChanges): void {
    //
  }
  ngOnDestroy(): void {
    if (this.queryParamSubscription) {
      this.queryParamSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.queryParamSubscription = combineLatest(
      this._activatedRoute.queryParams,
      this._activatedRoute.params
    ).subscribe((results) => {
      const queryParams = results[0];
      const params = results[1];
      // validate code
      if (queryParams.code == null) {
        // loading
        this._loading.showLoading();
        // prepare new item
        Promise.all([
          this._userService.getUser(),
          this._context.model('students/me').asQueryable()
            .expand('user', 'department', 'studyProgram($expand=studyLevel)', 'inscriptionMode', 'person').getItem(),
          this._context.model('DiningInscriptionModeTags').asQueryable()
            .expand('inscriptionModes').getItems()
        ]).then((results1) => {
          const user = results1[0];
          this.student = results1[1];

          // check student inscription mode          
          const inscriptionModeTags = results1[2];
          this.checkStudentInscriptionMode(inscriptionModeTags);

          // check max time limit for free dining
          this.checkMaxTimeLimit(this.student);

          // check student vat number
          this.checkStudentVatNumber();

          // check if dining card exists
          this.checkDiningCard();

          // get dining request event
          Promise.all([
            (this._context.model('DiningRequestEvents')
              .where('academicYear').equal(this.student.department.currentYear)
              .and('academicPeriod').equal(this.student.department.currentPeriod)
              .and('eventStatus/alternateName').equal('EventOpened')
              .expand('attachmentTypes($expand=attachmentType)').getItem())
          ]).then((results2) => {
            this.diningRequestEvent = results2[0];
            if (this.diningRequestEvent) {
              this.validatePeriod(this.diningRequestEvent);
            // check if student age is < 25yrs based on AcademicYear
              this.checkStudentAge(Number(this.diningRequestEvent.academicYear.name.split('-')[0]));
            }
            this.loading = false;
          });
          // set form
          this.formAction = params.action ? `DiningRequestActions/edit` : `DiningRequestActions/edit`;
          // and continue
          this._loading.hideLoading();
        }).catch((err) => {
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });
      } else {
        this._loading.showLoading();
        Promise.all([
          this._context.model('students/me').asQueryable()
            .expand('user', 'department', 'studyProgram', 'inscriptionMode', 'person').getItem(),
          this._context.model('DiningRequestActions').where('code').equal(queryParams.code)
            .expand('effectiveStatus', 'guardianFinancialAttributes','childrenFinancialAttributes','siblingFinancialAttributes', 'attachments($expand=attachmentType)', 'diningRequestEvent($expand=attachmentTypes($expand=attachmentType))', 'inscriptionModeTag').getItem()
        ]).then((results1) => {
          this.student = results1[0];
          const item = results1[1];
          if (item == null || this.student == null) {
            // tslint:disable-next-line:max-line-length
            return this._errorService.navigateToError(new ResponseError('The specified application cannot be found or is inaccessible', 404.5));
          }
          this.model = item;
          if (params.action !== 'preview' && this.model.actionStatus.alternateName !== 'PotentialActionStatus') {
            return this._router.navigate(['..', 'preview'], {
              queryParams: {
                code: this.model.code
              },
              relativeTo: this._activatedRoute
            });
          }

          // should have been provided for existing requests
          this.model.disableVatNumber = false;

          // check if dining card exists
          this.checkDiningCard();

          // get dining request event from model
          this.diningRequestEvent = this.model.diningRequestEvent;
          if (this.diningRequestEvent) {
            this.validatePeriod(this.diningRequestEvent);
            // check if student age is < 25yrs based on AcademicYear
            this.checkStudentAge(this.diningRequestEvent.academicYear);
          }
          this.loading = false;
          // set form
          this.formAction = (params.action === 'preview' || (this.model.effectiveStatus && this.model.effectiveStatus.alternateName === 'RejectedAttachmentsEffectiveStatus')) ? `DiningRequestActions/preview` : `DiningRequestActions/edit`;
          this.nextButtonDisabled = false;

          // fetch messages
          this.fetchMessages();
          // and continue
          this._loading.hideLoading();
        }).catch((err) => {
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });
      }
    });
  }

  private checkStudentVatNumber() {
    if (this.student.person.vatNumber != null) {
      this.model.studentAfm = this.student.person.vatNumber;
      //TODO should be disabled for production
      // this.model.disableVatNumber = true;
      this.model.disableVatNumber = false;
    }
    else {
      this.model.disableVatNumber = false;
    }
  }

  private checkStudentInscriptionMode(inscriptionModeTags) {
    // no model field is defined for qualification exams prerequisite so it is declared ad-hoc with a default value
    this.model.qualificationExams = false;

    inscriptionModeTags.forEach(inscriptionModeTag => {
      // check if any of the inscription modes of the tag matches the inscription mode of the student
      // the property name of the model MUST be the same as the alternate name of the tag 
      // to set it to true or false
      if (inscriptionModeTag.inscriptionModes.find(item => item.id == this.student.inscriptionMode.id)) {
        this.model[inscriptionModeTag.alternateName] = true;
      }
      else {
        this.model[inscriptionModeTag.alternateName] = false;
      }
    });
  }

  private checkMaxTimeLimit(student) {
    // determine the maximum time limit in semesters for active students who attend a study program
    // and their current semester is known. Return true if the student is beyond the maximum time 
    // limit for free dining, false otherwise.
    this.model.maxTimeLimit = true;

    if (student.studentStatus.alternateName === 'active') {
      if (student.studyProgram !== null && student.semester !== null) {
        let maxSemestersLimit = 0;
        const studySemesters = student.studyProgram.semesters;
        switch (student.studyProgram.studyLevel.alternateName)  {
          case 'undergraduate': maxSemestersLimit = studySemesters + 4; break;
          case 'postgraduate': maxSemestersLimit = studySemesters;
          case 'doctoral': maxSemestersLimit = 8; break;
          default: maxSemestersLimit = 0; break;
        }
        this.model.maxTimeLimit = student.semester > maxSemestersLimit;
      }
    }
  }

  private checkStudentAge(academicYear: number) {
    if (this.student.person.birthDate != null) {
      const birthDate = moment(this.student.person.birthDate, 'YYYY-MM-DD');
      const age = moment(new Date(academicYear, 8, 1)).diff(birthDate, 'years');
      if (age < 25) {
        this.model.lessThan25yrs = true;
      } else {
        this.model.lessThan25yrs = false;
      }
      this.model.disableLessThan25yrsField = true;
    } else {
      this.model.disableLessThan25yrsField = false;
    }
  }

  checkDiningCard(): void {
    // check if student dining card exists
    this._context.model('StudentDiningCards')
      .where('student').equal(this.student.id)
      .and('academicYear').equal(this.student.department.currentYear)
      .and('academicPeriod').equal(this.student.department.currentPeriod)
      .and('active').equal(true).getItem().then((card) => {
        if (card != null) {
          this.studentDiningCard = true;
        }
      }).catch((err) => {
        window.location.reload();
        this._loading.hideLoading();
      });
  }

  fetchMessages(): void {
    if (this.model && this.model.id) {
      this._context.model(`DiningRequestActions/${this.model.id}/messages`).asQueryable()
        .orderBy('dateCreated desc').expand('attachments').getItems().then((messages) => {
          this.messages = messages;
        }).catch((err) => {
          console.error(err);
          this.messages = [];
        });
    } else {
      this.messages = [];
    }
  }

  ngAfterViewInit(): void {
    //
  }

  onDataChange(event: any): void {

 }

  async beforeNext(nextTab: TabDirective): Promise<boolean> {

    try {
      if (nextTab.id === 'personal-information') {
        return true;
      }
      // save application before attach documents
      if (nextTab.id === 'attach-documents') {
        // save action
        if (this.model.actionStatus.alternateName === 'PotentialActionStatus') {
          this._loading.showLoading();
          // set additional data
          const data = this.form1.form.formio.data;

          //TO BE REMOVED
          if(!data.studentOrphan){
            data.studentOrphan = false;
          }
          if(!data.maritalStatus){
            data.maritalStatus = null;
          }
          if(!data.numberOfStudyingSiblings){
            data.numberOfStudyingSiblings = null;
          }
          if(Object.prototype.hasOwnProperty.call(data, 'siblingFinancialAttributes') === false){
            data.siblingFinancialAttributes = [];
          }
          if(Object.prototype.hasOwnProperty.call(data, 'childrenFinancialAttributes') === false){
            data.childrenFinancialAttributes = [];
          }
          if(Object.prototype.hasOwnProperty.call(data, 'guardianFinancialAttributes') === false){
            data.guardianFinancialAttributes = [];
          }
          if(Object.prototype.hasOwnProperty.call(data, 'noOfChildren') === false){
            data.noOfChildren = 0;
          }

          Object.assign(data, {
            agent: null,
            diningRequestEvent: this.diningRequestEvent
          });
          // save application
          const result = await this._context.model('DiningRequestActions').save(data);
          // update current action
          Object.assign(this.model, result);
          // and continue
          this._loading.hideLoading();
        }
      }
      return true;
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
      return false;
    }
  }

  next(): void {
    // get active tab
    const findIndex = this.tabset.tabs.findIndex((tab) => {
      return tab.active;
    });
    if (findIndex < this.tabset.tabs.length) {
      // set active tab
      const tab = this.tabset.tabs[findIndex + 1];
      this.beforeNext(tab).then((result) => {
        if (result) {
          tab.disabled = false;
          tab.active = true;
        }
      });
    }
  }

  onCustomEvent(event: any): void {
    if (event.type === 'nextEvent') {
      this.next();
    }
  }

  validatePeriod(event: { validFrom?: Date; validThrough?: Date }): void {
    const now = new Date();
    let valid = false;
    let started = false;
    if (event.validFrom instanceof Date) {
      if (event.validThrough instanceof Date) {
        valid = event.validFrom <= now && event.validThrough > now;
      } else {
        valid = event.validFrom < now;
      }
      if (event.validFrom <= now) {
        started = true;
      }
    } else if (event.validThrough instanceof Date) {
      valid = event.validThrough > now;
      if (event.validThrough > now) {
        started = true;
      }
    }
    Object.assign(event, {
      valid,
      started
    });
  }

  onFileSelect(event, attachmentType): any {
    // get file
    const addedFile = event.addedFiles[0];
    const formData: FormData = new FormData();
    formData.append('file', addedFile, addedFile.name);
    formData.append('attachmentType', attachmentType.id);
    formData.append('published', 'true');
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/addAttachment`);
    this._loading.showLoading();
    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    }).subscribe((result) => {
      // reload attachments
      this._context.model(`DiningRequestActions/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onFileRemove(attachment: any): any {
    this._loading.showLoading();
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/removeAttachment`);
    return this._http.post(postUrl, attachment, {
      headers: serviceHeaders
    }).subscribe(() => {
      // reload attachments
      this._context.model(`DiningRequestActions/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onSelectAttachDocuments(): void {
    this.documents = this.documents || [];
    // if study program enrollment event is not defined
    if (this.diningRequestEvent == null) {
      // exit
      return;
    }
    // if guardianType is parents then ask financial data for both
    if (this.model.guardianType){
      if (this.model.guardianType.alternateName === 'parents' && this.model.studentParentsFileJointTaxReport === false){
        this.askMoreAttachmentsForIncomeParents = true;
        this.moreAttachmentNumberForIncomeParents  = this.model.guardianFinancialAttributes.length;
      }
    }
    // if studentSiblings file tax report then ask financial documents
    if(this.model.maritalStatus && this.model.guardianNoOfChildrenLessThan18yrs){
      if(this.model.maritalStatus.alternateName === 'single' && this.model.guardianNoOfChildrenLessThan18yrs > 0){
          this.askMoreAttachmentsForIncomeSiblings = true;
          this.moreAttachmentNumberForIncomeSiblings = this.model.siblingFinancialAttributes.filter((el)=>el.hasTaxDeclaration === true).length;
      }
    }

    // if studentChildren file tax report then ask financial documents
    if(this.model.maritalStatus && this.model.noOfChildren){
      if(this.model.maritalStatus.alternateName === 'married' && this.model.noOfChildren !== 0){
         this.askMoreAttachmentsForIncomeStudentChildren = true;
         this.moreAttachmentNumberStudentChildren = this.model.childrenFinancialAttributes.filter((el)=>el.hasTaxDeclaration === true).length;
      }
  }

    this.documents = this.diningRequestEvent.attachmentTypes.map((item: any) => {

      const res = {
        attachmentType: item.attachmentType,
        numberOfAttachments: this.checkNumberOfAttachments(item)? this.checkNumberOfAttachments(item) :  item.numberOfAttachmentsProperty !== null? this.model[item.numberOfAttachmentsProperty]: item.numberOfAttachments,
        requiredRule: item.requiredRule
      };
      
      // try to search model attachments list
      const findAttachments = this.model.attachments.filter((attachment) => {
        if (attachment.attachmentType == null || attachment.published === false) {        
          return false;
        }
        return attachment.attachmentType.id === item.attachmentType.id;
      });
      Object.assign(res, {
        attachments: findAttachments
      });
      return res;
    }).filter(item => {

      console.log(item.attachmentType.alternateName + ' ' + item.requiredRule);

      if (item.requiredRule) {
        // debugging purposes code 
        // const executeTemplate = template;
        // const model = this.model;

        // determine if the attachment is required or not
        try {
          // debugging purposes code 
          // const result: string = executeTemplate('${' + item.requiredRule + '}')(model);
          const result: string = template('${' + item.requiredRule + '}')(this.model);
          const numberOfAttachments =  result === 'true' ? item.numberOfAttachments : 0;
          if (numberOfAttachments === 0) {
            return false;
          }
          return true;
        } catch (err) {
          console.log(err);
          return false;
        }
      }
      return false;
    });
    this.documentStateChange.emit(this.documentState);
  }

  submit(): any {
    const SubmitTitle = this._translateService.instant('UniversisDiningModule.ModalConfirm.Title');
    const SubmitTitleMessage = this._translateService.instant('UniversisDiningModule.ModalConfirm.Body');
    this._modal.showSuccessDialog(SubmitTitle, SubmitTitleMessage).then((result) => {
      if (result === 'ok') {
        this._loading.showLoading();
        const model = cloneDeep(this.model);
        // remove attachments
        delete model.attachments;
        // set action status
        model.actionStatus = {
          alternateName: 'ActiveActionStatus'
        };
        // set effective status
        model.effectiveStatus = {
          alternateName: 'AcceptedAttachmentsEffectiveStatus'
        };
        this._context.model('DiningRequestActions').save(model).then(() => {
          // navigate to list
          this._loading.hideLoading();
          return this._router.navigate([
            '/'
          ]);
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  onSelectTab(event: any): void {
    if(this.model.actionStatus.alternateName != 'PotentialActionStatus'){
      return;
    }
    const find = this.tabset.tabs.find((tab) => {
      return tab.id === 'attach-documents';
    });
    if (find) {
      find.disabled = true;
    }
  }

  downloadAttachment(attachment: any): void {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loading.showLoading();
    fetch(fileURL, {
      headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status !== 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loading.hideLoading();
      }).catch((err) => {
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }


  async send(message: any): Promise<void> {
    try {
      this._loading.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        student: {
          id: this.student.id
        },
        recipient: {
          name: 'Registrar'
        }
      });
      await this._context.model(`DiningRequestActions/${this.model.id}/messages`).save(message);
      // reload message
      this.fetchMessages();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  checkNumberOfAttachments(item: any){
    var numberOfAttachments;
    // student not Cyprus or Foreign
    if(this.model.cyprusStudent !== true && this.model.foreignStudentOrChildOfGreekExpatriate !== true){
      switch(item.attachmentType.alternateName) {
        case 'studentParentsTaxIncomeDocument':
        case 'studentParentsTaxE1Document':
          if(this.askMoreAttachmentsForIncomeParents === true ){
            numberOfAttachments = this.moreAttachmentNumberForIncomeParents;
          } 
          break;
        case 'studentChildrenTaxIncomeDocument':
        case 'studentChildrenTaxE1Document': 
          if(this.askMoreAttachmentsForIncomeStudentChildren === true){
           numberOfAttachments = this.moreAttachmentNumberStudentChildren;
          }
          break;
        case 'studentSiblingTaxIncomeDocument':
        case 'studentSiblingTaxE1Document':
          if(this.askMoreAttachmentsForIncomeSiblings === true){
          numberOfAttachments = this.moreAttachmentNumberForIncomeSiblings;
          } 
          break; 
      } 
    } 

// Cyprus student
    if(this.model.cyprusStudent === true && this.model.foreignStudentOrChildOfGreekExpatriate !== true){
     switch(item.attachmentType.alternateName) {
       case 'familyCyprusIncomeCertificate': 
         if(this.askMoreAttachmentsForIncomeParents === true && this.model.cyprusStudent == true && this.model.foreignStudentOrChildOfGreekExpatriate !== true){
           numberOfAttachments = this.moreAttachmentNumberForIncomeParents;
         } 
         break; 
        case 'siblingsCyprusIncomeCertificate':
          if(this.askMoreAttachmentsForIncomeSiblings === true){
            numberOfAttachments = this.moreAttachmentNumberForIncomeSiblings;
            } 
         break;
        case 'studentChildrenCyprusIncomeCertificate':
          if(this.askMoreAttachmentsForIncomeSiblings === true){
            numberOfAttachments = this.moreAttachmentNumberStudentChildren;
            } 
         break;
     }
    }

// foreign Student Or Child Of Greek Expatriate
    if(this.model.cyprusStudent !== true && this.model.foreignStudentOrChildOfGreekExpatriate === true){
     switch(item.attachmentType.alternateName) {
       case 'foreignFamilyIncomeCertificate': 
         if(this.askMoreAttachmentsForIncomeParents === true && this.model.cyprusStudent !== true && this.model.foreignStudentOrChildOfGreekExpatriate === true){
           numberOfAttachments = this.moreAttachmentNumberForIncomeParents;
         } 
         break; 
       case 'studentChildrenForeignIncomeCertificate': 
         if(this.askMoreAttachmentsForIncomeStudentChildren === true){
          numberOfAttachments = this.moreAttachmentNumberStudentChildren;
         }
         break; 
       case 'studentSiblingForeignIncomeCertificate':
        if(this.askMoreAttachmentsForIncomeSiblings === true){
        numberOfAttachments = this.moreAttachmentNumberForIncomeSiblings;
        } 
         break; 
     }
    }
    return numberOfAttachments;
  }

}
