import { NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { DiningComponent } from './dining.component';
import { DiningRoutingModule } from './dining-routing.module';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DiningRoutingModule,    
    FormsModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    TabsModule.forRoot(),
    NgArrayPipesModule,
    NgxDropzoneModule      
  ],
  declarations: [DiningComponent, ComposeMessageComponent],
  exports: [DiningComponent]
})
export class DiningModule { //implements OnInit {
  constructor() {
  }
}