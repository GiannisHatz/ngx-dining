/*
 * Public API Surface of dining
 */

export * from './lib/dining.service';
export * from './lib/dining.component';
export * from './lib/dining.module';
export * from './lib/dining-routing.module';
