import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvancedFormItemResolver, AdvancedFormModalData } from '@universis/forms';
import { AdvancedListComponent } from '@universis/ngx-tables';
import { DiningCardListConfiguration } from './components/dining-card-list.config';
import { DiningCardSearchConfiguration } from './components/dining-card-search.config';
import { DiningListConfiguration } from './components/dining-list.config';
import { DiningSearchConfiguration } from './components/dining-search.config';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModalEditCardComponent } from './components/edit-card/modal-edit-card/modal-edit-card.component';
import { EditComponent } from './components/edit/edit.component';
import { ModalEditComponent } from './components/edit/modal-edit.component';

const routes: Routes = [
    {
        'path': '',
        'pathMatch': 'full',
        'redirectTo': 'index'
    },
    {
        path: 'index',
        component: AdvancedListComponent,
        data: {
            model: 'DiningRequestActions',
            description: 'NewRequestTemplates.DiningRequestAction.Description',
            tableConfiguration: DiningListConfiguration,
            searchConfiguration: DiningSearchConfiguration
        },
        children: [
          {
            path: 'item/:id/edit',
            component: ModalEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiningRequestActions',
              action: 'modal-edit',
              serviceQueryParams: EditComponent.ServiceQueryParams,
              modalOptions: {
                modalClass: 'mx-w-90'
              },
              closeOnSubmit: true,
              continueLink: '.'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          }
        ]
    },
    {
      path: 'dining-cards',
      component: AdvancedListComponent,
      data: {
          model: 'StudentDiningCards',
          description: 'UniversisDiningModule.DiningCardsTemplates.DiningCard.Description',
          tableConfiguration: DiningCardListConfiguration,
          searchConfiguration: DiningCardSearchConfiguration 
      },
      children: [
        {
          path: 'item/:id/edit',
          component: ModalEditCardComponent,
          outlet: 'modal',
          data: <AdvancedFormModalData>{
            model: 'StudentDiningCards',
            action: 'modal-edit',
            serviceQueryParams: EditCardComponent.ServiceQueryParams,
            modalOptions: {
              modalClass: 'mx-w-90'
            },
            closeOnSubmit: true,
            continueLink: '.'
          },
          resolve: {
            model: AdvancedFormItemResolver
          }
        }
      ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminDiningRoutingModule { }
