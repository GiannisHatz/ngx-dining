import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { AdminDiningRoutingModule } from './admin-routing.module';
import { TablesModule } from '@universis/ngx-tables';

import { ModalEditComponent } from './components/edit/modal-edit.component';
import { EditComponent } from './components/edit/edit.component';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { SidePreviewComponent } from './components/side-preview/side-preview.component';
import { AttachmentDownloadComponent } from './components/attachment-download/attachment-download.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ListComponent } from './components/list/list.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModalEditCardComponent } from './components/edit-card/modal-edit-card/modal-edit-card.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AdminDiningRoutingModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    TablesModule,
    NgxExtendedPdfViewerModule    
  ],
  declarations: [
    ModalEditComponent,
    EditComponent,
    ComposeMessageComponent,
    SidePreviewComponent,
    AttachmentDownloadComponent,
    ListComponent,
    EditCardComponent,
    ModalEditCardComponent,
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminDiningModule {  
}
