export const DiningSearchConfiguration = {
    "components": [
      {
        "label": "Columns",
        "columns": [
          {
            "components": [
              {
                "label": "Students.StudentIdentifier",
                "spellcheck": true,
                "tableView": true,
                "validate": {
                  "unique": false,
                  "multiple": false
                },
                "key": "studentIdentifier",
                "type": "textfield",
                "input": true,
                "hideOnChildrenHidden": false
              }
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components": [
              {
                "label": "Students.FullName",
                "spellcheck": true,
                "tableView": true,
                "validate": {
                  "unique": false,
                  "multiple": false
                },
                "key": "studentName",
                "type": "textfield",
                "input": true,
                "hideOnChildrenHidden": false
              }
            ],
            "width": 5,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components": [
              {
                "label": "Requests.ActionStatusTitle",
                "widget": "choicesjs",
                "dataSrc": "url",
                "data": {
                  "url": "ActionStatusTypes",
                  "headers": []
                },
                "selectThreshold": 0.3,
                "validate": {
                  "unique": false,
                  "multiple": false
                },
                "template": "{{ item.alternateName }}",
                "key": "actionStatus",
                "valueProperty": "alternateName",
                "selectValues": "value",
                "type": "select",
                "input": true,
                "hideOnChildrenHidden": false,
                "disableLimit": false,
                "lazyLoad": false
              }
            ],
            "width": 4,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          
          {
            "components": [
              {
                "label": "Requests.StudentStatus",
                "widget": "choicesjs",
                "dataSrc": "url",
                "data": {
                  "url": "StudentStatuses?$orderby=name",
                  "headers": []
                },
                "selectThreshold": 0.3,
                "validate": {
                  "unique": false,
                  "multiple": false
                },
                "template": "{{ item.alternateName }}",
                "key": "studentStatus",
                "valueProperty": "alternateName",
                "selectValues": "value",
                "type": "select",
                "input": true,
                "hideOnChildrenHidden": false,
                "disableLimit": false,
                "lazyLoad": false
              }
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components": [
              {
              "label": "Requests.RequestedAfter",
                "tableView": false,
                "enableTime": false,
                "enableMinDateInput": false,
                "datePicker": {
                    "disableWeekends": false,
                    "disableWeekdays": false
                },
                "enableMaxDateInput": false,
                "key": "minDate",
                "type": "datetime",
                "input": true,
                "widget": {
                    "type": "calendar",
                    "displayInTimezone": "viewer",
                    "locale": "en",
                    "useLocaleSettings": false,
                    "allowInput": true,
                    "mode": "single",
                    "enableTime": true,
                    "noCalendar": false,
                    "format": "yyyy-MM-dd",
                    "hourIncrement": 1,
                    "minuteIncrement": 1,
                    "time_24hr": true,
                    "disableWeekends": false,
                    "disableWeekdays": false
                }
              }
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components": [
              {
                "label": "Requests.RequestedBefore",
                "tableView": false,
                "enableMinDateInput": false,
                "enableTime": false,
                "datePicker": {
                    "disableWeekends": false,
                    "disableWeekdays": false
                },
                "enableMaxDateInput": false,
                "key": "maxDate",
                "type": "datetime",
                "input": true,
                "widget": {
                    "type": "calendar",
                    "displayInTimezone": "viewer",
                    "useLocaleSettings": true,
                    "allowInput": true,
                    "mode": "single",
                    "enableTime": true,
                    "noCalendar": false,
                    "time_24hr": false,
                    "disableWeekends": false,
                    "disableWeekdays": false
                }
              }           
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components" : [  
              {
                "input": true,
                "label": "Forms.DiningRequest.LessThan25yrs",
                "key": "lessThan25yrs",
                "widget": "choicesjs",
                "type": "select",
                "data": {
                    "values": [                    
                        {
                        "value": "true",
                        "label": "Yes"
                        },
                        {
                        "value": "false",
                        "label": "No"
                        }
                    ]
                },
                "dataType" : "boolean"
              }   
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components" : [  
              {
                "input": true,
                "label": "Forms.DiningRequest.LivePermanentlyInSameLocationWithInstitution",
                "key": "livePermanentlyInSameLocationWithInstitution",
                "widget": "choicesjs",
                "type": "select",
                "data": {
                    "values": [                    
                        {
                        "value": "true",
                        "label": "Yes"
                        },
                        {
                        "value": "false",
                        "label": "No"
                        }
                    ]
                },
                "dataType" : "boolean"
              }   
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components" : [  
              {
                "input": true,
                "label": "Forms.DiningRequest.ForeignScholarStudent",
                "key": "foreignScholarStudent",
                "widget": "choicesjs",
                "type": "select",
                "data": {
                    "values": [                    
                        {
                        "value": "true",
                        "label": "Yes"
                        },
                        {
                        "value": "false",
                        "label": "No"
                        }
                    ]
                },
                "dataType" : "boolean"
              }   
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components" : [  
              {
                "input": true,
                "label": "Forms.DiningRequest.Prerequisites",
                "key": "diningPrerequisites",
                "widget": "choicesjs",
                "type": "select",
                "data": {
                    "values": [                    
                        {
                        "value": "true",
                        "label": "Yes"
                        },
                        {
                        "value": "false",
                        "label": "No"
                        }
                    ]
                },
                "dataType" : "boolean"
              }   
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components" : [  
              {
                "input": true,
                "label": "Forms.DiningRequest.Unemployment",
                "key": "unemployment",
                "widget": "choicesjs",
                "type": "select",
                "data": {
                    "values": [                    
                        {
                        "value": "true",
                        "label": "Yes"
                        },
                        {
                        "value": "false",
                        "label": "No"
                        }
                    ]
                },
                "dataType" : "boolean"
              }   
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components": [
              {
                "label": "Forms.DiningRequest.StudentMaritalStatus",
                "widget": "choicesjs",
                "dataSrc": "url",
                "data": {
                  "url": "FamilyStatuses?$orderby=name",
                  "headers": []
                },
                "filter":"$filter=(alternateName eq 'single') or (alternateName eq 'married')",
                "validate": {
                  "unique": false,
                  "multiple": false
                },
                "template": "{{ item.alternateName }}",
                "key": "studentMaritalStatus",
                "valueProperty": "alternateName",
                "selectValues": "value",
                "type": "select",
                "input": true,
                "hideOnChildrenHidden": false,
                "disableLimit": false,
                "lazyLoad": false
              }
            ],
            "width": 3,
            "offset": 0,
            "push": 0,
            "pull": 0
          }
        ],
        "tableView": false,
        "key": "columns1",
        "type": "columns",
        "input": false,
        "path": "columns1"
      }
    ]
  }
  