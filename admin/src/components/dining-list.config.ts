// tslint:disable: quotemark
export const DiningListConfiguration = {
  "title": "Requests.ActiveStudentRequestActions",
  "model": "DiningRequestActions",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "(indexof(student/person/familyName, '${text}') ge 0 or indexof(student/person/givenName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0)",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          {
            "outlets": {
              "modal": ["item", "${id}", "edit"]
            }
          }
        ]
      }
    },
    {
      "name": "student/studentIdentifier",
      "property": "studentIdentifier",
      "title": "Requests.StudentIdentifier"
    },
    {
      "name": "student/person/familyName",
      "property": "familyName",
      "title": "Requests.FullName",
      "formatter": "TemplateFormatter",
      "formatString": "${familyName} ${givenName}"
    },
    {
      "name": "student/person/givenName",
      "property": "givenName",
      "title": "Requests.GivenName",
      "hidden": true
    },
    {
      "name": "student/person/fatherName",
      "property": "studentFatherName",
      "title": "Requests.FatherName"
    },
    {
      "name": "student/person/motherName",
      "property": "studentMotherName",
      "title": "Requests.MotherName"
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatter": "TranslationFormatter",
      "formatString": "StudentStatuses.${value}"
    },
    {
      "name": "startTime",
      "property": "startTime",
      "title": "Requests.StartTime",
      "formatter": "DateTimeFormatter",
      "formatString": "shortDate"
    },
    {
      "name": "actionStatus/alternateName",
      "property": "actionStatus",
      "title": "Requests.ActionStatusTitle",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "ActionStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus'",
              "text-primary": "'${actionStatus}'==='ActiveActionStatus' || '${actionStatus}'==='PotentialActionStatus'",
              "text-success": "'${actionStatus}'==='CompletedActionStatus'"
            }
          }
        }
      ]
    }
  ],
  "defaults": {
    "orderBy": "dateCreated desc"
  },
  "paths": [
    {
      "name": "Requests.Active",
      "show": true,
      "alternateName": "list/active",
      "filter": {
      }
    },
    {
      "name": "Requests.All",
      "show": true,
      "alternateName": "list/index",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.ExamPeriodParticipateAction",
      "show": true,
      "alternateName": "list/examParticipate",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.RequestDocumentActions",
      "show": true,
      "alternateName": "list/documentAction",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.RequestsRemoveAction",
      "show": false,
      "alternateName": "list/remove",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.RequestsSuspendAction",
      "show": false,
      "alternateName": "list/suspend",
      "filter": {
      }
    },
    {
      "name": "Requests.AllDepartments",
      "show": true,
      "alternateName": "list/allDepartments",
      "filter": {
      }
    }
  ],
  "criteria": [
    {
      "name": "lessThan25yrs",
      "filter": "(lessThan25yrs eq ${value})",
      "type": "text"
    },
    {
      "name": "livePermanentlyInSameLocationWithInstitution",
      // tslint:disable-next-line: max-line-length
      "filter": "(institutionOrClubLocationSameAsStudentPermanentResidence eq ${value} or institutionOrClubLocationSameAsGuardianPermanentResidence eq ${value})",
      "type": "text"
    },
    {
      "name": "foreignScholarStudent",
      "filter": "(foreignScholarStudent eq ${value})",
      "type": "text"
    },
    {
      "name": "diningPrerequisites",
      // tslint:disable-next-line: max-line-length
      "filter": "(militaryActive ne ${value} and militarySchool ne ${value} and studentHouseResident ne ${value} and sameDegreeHolder ne ${value})",
      "type": "text"
    },
    {
      "name": "unemployment",
      // tslint:disable-next-line: max-line-length
      "filter": "(studentSpouseReceivesUnemploymentBenefit eq ${value} or studentGuardianReceivesUnemploymentBenefit eq ${value} or studentReceivesUnemploymentBenefit eq ${value})",
      "type": "text"
    },
    {
      "name": "studentMaritalStatus",
      "filter": "(maritalStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "additionalType",
      "filter": "(additionalType eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentName",
      "filter": "(indexof(student/person/familyName, '${value}') ge 0 or indexof(student/person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentStatus",
      "filter": "(student/studentStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "actionStatus",
      "filter": "(actionStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentIdentifier",
      "filter": "(indexof(student/studentIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "startTime",
      "filter": "(startTime eq '${value}')",
      "type": "text"
    },
    {
      "name": "minDate",
      "filter": "(dateCreated ge '${new Date(value).toISOString()}')"
    },
    {
      "name": "maxDate",
      "filter": "(dateCreated le '${new Date(value).toISOString()}')"
    }
  ],
  "searches": []
};