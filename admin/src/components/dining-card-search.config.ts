export const DiningCardSearchConfiguration = {
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Students.StudentIdentifier",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentIdentifier",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.FullName",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 5,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.StudentDepartment",
              "labelPosition": "top",
              "widget": "choicesjs",
              "key": "studentDepartment",
              "dataSrc": "url",
              "data": {
                "url": "LocalDepartments?$top=-1&$skip=0&$orderby=name",
                "headers": []
              },
              "template": " {{item.id}} - {{item.name}} ",
              "selectValues": "value",
              "valueProperty": "name",
              "type": "select",
              "input": true,
              "disabled": false,
              "lazyLoad": false,
              "searchEnabled": true
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.StudentCategory",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "StudentCategories?$orderby=name",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.name }}",
              "key": "studentCategory",
              "valueProperty": "name",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.StudentStatus",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "StudentStatuses?$orderby=name",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.name }}",
              "key": "studentStatus",
              "valueProperty": "alternateName",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "input": true,
              "label": "UniversisDiningModule.DiningCardsTemplates.active",
              "key": "active",
              "widget": "choicesjs",
              "type": "select",
              "data": {
                "values": [
                  {
                    "value": "true",
                    "label": "DiningCardStatuses.Active"
                  },
                  {
                    "value": "false",
                    "label": "DiningCardStatuses.Cancelled"
                  }
                ]
              },
              "dataType": "boolean",
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.DiningCardsTemplates.AcademicYear",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "AcademicYears?$top={{limit}}&$skip={{skip}}&$orderby=id desc",
                "headers": []
              },
              "searchFields": ["id", "name"],
              "template": "<span class='d-block pr-5 text-truncate'>{{ item.name }}</span>",
              "selectValues": "value",
              "key": "academicYear",
              "valueProperty": "alternateName",
              "type": "select",
              "input": true,
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.DiningCardsTemplates.AcademicPeriod",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "AcademicPeriods/?$select=id,name,alternateName",
                "headers": []
              },
              "searchFields": ["id", "name"],
              "template": "<span class='d-block pr-5 text-truncate'>{{ item.alternateName }}</span>",
              "selectValues": "value",
              "key": "academicPeriod",
              "valueProperty": "alternateName",
              "type": "select",
              "input": true,
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.DiningCardsTemplates.validFrom",
              "tableView": false,
              "enableTime": false,
              "enableValidFromInput": false,
              "datePicker": {
                "disableWeekends": false,
                "disableWeekdays": false
              },
              "enableMaxDateInput": false,
              "key": "validFrom",
              "type": "datetime",
              "input": true,
              "widget": {
                "type": "calendar",
                "displayInTimezone": "viewer",
                "locale": "en",
                "useLocaleSettings": false,
                "allowInput": true,
                "mode": "single",
                "enableTime": true,
                "noCalendar": false,
                "format": "yyyy-MM-dd",
                "hourIncrement": 1,
                "minuteIncrement": 1,
                "time_24hr": true,
                "disableWeekends": false,
                "disableWeekdays": false
              }
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.DiningCardsTemplates.validThrough",
              "tableView": false,
              "enableTime": false,
              "enableMinDateInput": false,
              "datePicker": {
                "disableWeekends": false,
                "disableWeekdays": false
              },
              "enableMaxDateInput": false,
              "key": "validThrough",
              "type": "datetime",
              "input": true,
              "widget": {
                "type": "calendar",
                "displayInTimezone": "viewer",
                "locale": "en",
                "useLocaleSettings": false,
                "allowInput": true,
                "mode": "single",
                "enableTime": true,
                "noCalendar": false,
                "format": "yyyy-MM-dd",
                "hourIncrement": 1,
                "minuteIncrement": 1,
                "time_24hr": true,
                "disableWeekends": false,
                "disableWeekdays": false
              }
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    }
  ]
}
