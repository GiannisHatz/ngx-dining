// tslint:disable: quotemark
export const DiningCardListConfiguration = {
  "title": "DiningCards",
  "model": "StudentDiningCards",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "(indexof(student/person/familyName, '${text}') ge 0 or indexof(student/person/givenName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0)",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          {
            "outlets": {
              "modal": ["item", "${id}", "edit"]
            }
          }
        ]
      }
    },
    {
      "name": "student/studentIdentifier",
      "property": "studentIdentifier",
      "title": "Students.StudentIdentifier"
    },
    {
      "name": "student/person/familyName",
      "property": "familyName",
      "title": "Requests.FullName",
      "formatter": "TemplateFormatter",
      "formatString": "${familyName} ${givenName}"
    },
    {
      "name": "student/person/givenName",
      "property": "givenName",
      "title": "Requests.GivenName",
      "hidden": "true"
    },
    {
      "name": "student/person/fatherName",
      "property": "studentFatherName",
      "title": "Requests.FatherName"
    },
    {
      "name": "student/person/motherName",
      "property": "studentMotherName",
      "title": "Requests.MotherName"
    },
    {
      "name": "student/department/abbreviation",
      "property": "studentDepartmentAbbreviation",
      "title": "Students.DepartmentAbbreviation",
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatter": "TranslationFormatter",
      "formatString": "StudentStatuses.${value}"
    },
    {
      "name": "student/category/name",
      "property": "studentCategory",
      "title": "Students.StudentCategory"
    },
    {
      "name": "validFrom",
      "title": "UniversisDiningModule.DiningCardsTemplates.validFrom",
      "filter": "(validFrom eq '${value}')",
      "type": "text",
      "formatter": "DateTimeFormatter",
      "formatString": "shortDate"
    },
    {
      "name": "serialNumber",
      "filter": "(indexof(student/studentIdentifier, '${value}') ge 0)",
      "type": "text",
      "hidden": "true"
    },
    {
      "name": "active",
      "title": "UniversisDiningModule.DiningCardsTemplates.active",
      "show": true,
      "type": "boolean",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "UniversisDiningModule.DiningCardStatuses.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${active}'==='false'",
              "text-primary": "'${active}'==='true'"
            }
          }
        }
      ]
    },
  ],
  "criteria": [
    {
      "name": "studentIdentifier",
      "filter": "(indexof(student/studentIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentName",
      "filter": "(indexof(student/person/familyName, '${value}') ge 0 or indexof(student/person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentStatus",
      "filter": "(student/studentStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentCategory",
      "filter": "(studentCategory/name eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentDepartment",
      "filter": "(indexof(student/department/name,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "academicYear",
      "filter": "(indexof(academicYear/alternateName,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "academicPeriod",
      "filter": "(indexof(academicPeriod/alternateName,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "validFrom",
      "filter": "(validFrom ge '${new Date(value).toISOString()}')",
      "type": "text"
    },
    {
      "name": "validThrough",
      "filter": "(validThrough le '${new Date(value).toISOString()}')",
      "type": "text"
    },
  ],
  "searches": []
};


