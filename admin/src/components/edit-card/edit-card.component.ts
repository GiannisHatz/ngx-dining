import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { DataServiceQueryParams, ResponseError } from '@themost/client';
import { ModalService, LoadingService, ErrorService, AppEventService, DIALOG_BUTTONS } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'lib-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.css']
})
export class EditCardComponent implements OnInit {
  public static readonly ServiceQueryParams = {
    $expand: 'student($expand=person)'
  };

  dataSubscription: any;
  paramSubscription: any;
  editingReview: any = false;
  @ViewChild('form') form: AdvancedFormComponent;
  @Input() model: any;
  @Input() showNavigation = true;
  @Input() showActions = true;
  public modalRef: BsModalRef;
  private active: boolean;
  // public messages: any[];
  public otherApplications: any[];
  newMessage: any = {
    attachments: []
  };
  public showNewMessage = false;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _modal: ModalService,
    private _modalService: BsModalService,
    private _loading: LoadingService,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _http: HttpClient,
    private _appEvent: AppEventService) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((query) => {
      //
    });
    this.dataSubscription = this._activatedRoute.data.subscribe((data) => {
      this.model = data.model;
      if (this.form) {
        this.form.refreshForm.emit({
          submission: {
            data: this.model
          }
        });
      }
    });
  }

  reload() {
    if (this.model == null) {
      // do nothing
      return;
    }
    return this._context.model('StudentDiningCards')
      .asQueryable(<DataServiceQueryParams>EditCardComponent.ServiceQueryParams)
      .where('id').equal(this.model.id)
      .getItem().then((result) => {
        this.model = result;
      });
  }


  reset(templateToLoad: TemplateRef<any>) {
    this.modalRef = this._modalService.show(templateToLoad);
  }

  // confirm callback of the cancel card modal 
  // user accepted to cancel dining card => reject active state and reset cancelReason and close modal
  public confirmCancelCardModal(cancelReason: string) {
    debugger
    this.active = false;
    this.model.active = this.active;
    this.model.cancelReason = cancelReason;
    if (cancelReason) {
      debugger
      this.modalRef.hide();
      const headers = new Headers();
      const serviceHeaders = this._context.getService().getHeaders();
      // cancel dining card
      console.log("🚀 ~ file: edit-card.component.ts ~ line 166 ~ EditCardComponent ~ confirmCancelCardModal ~ this.model.id", this.model.id)
      const postUrl = this._context.getService().resolve(`StudentDiningCards/${this.model.id}`);

      this._loading.showLoading();
      return this._http.post(postUrl, this.model, {
        headers: serviceHeaders
      }).subscribe(async (result) => {
        if (result) {
          // notify user about reason of rejection by message                 
          const message = {
            subject: this._translateService.instant('UniversisDiningModule.CancelCardSubject'),
            body: this._translateService.instant('UniversisDiningModule.CancelCardBody', { cancelReason: cancelReason })
          };
          // await this.sendWithoutAttachment(message);
          // clear 
          this.active = null;
        }
        // reload page
        return this.reload().then(() => {
          this._loading.hideLoading();
          // send an application event
          this._appEvent.change.next({
            model: 'StudentDiningCards',
            target: this.model
          });
        }).catch((err) => {
          this._loading.hideLoading();
          const ReloadError = this._translateService.instant('Register.ReloadError') || {
            'Title': 'Refresh failed',
            'Message': 'The operation has been completed successfully but something went wrong during refresh.'
          };
          this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
            this._router.navigate(['/requests/dining']);
          });
        });
      }, (err) => {
        this._loading.hideLoading();
        // clear
        this.active = null;
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    } else {
      // User did not insert any text
      alert(this._translateService.instant('UniversisDiningModule.CancelCardModal.TitleOnUserError'));
    }
  }

  public reactivate() {
    // todo: reactivate card
  }

  public cancel() {
    // todo: cancel card
  }

  // cancel callback of the reject attachment modal 
  // user declined rejection => close modal
  // public closeRejectAttachmentModal(): void {
  //   this.modalRef.hide();
  // }

  // revertAttachment(attachment: any) {
  //   // revert (accept back) attachment
  //   const headers = new Headers();
  //   const serviceHeaders = this._context.getService().getHeaders();    
  //   const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/revertAttachment`);
  //   this._loading.showLoading();
  //   return this._http.post(postUrl, attachment, {
  //     headers: serviceHeaders
  //   }).subscribe(async (result) => {
  //       // reload page
  //       return this.reload().then(() => {
  //         this._loading.hideLoading();
  //         // send an application event
  //         this._appEvent.change.next({
  //           model: 'DiningRequestActions',
  //           target: this.model
  //         });
  //       }).catch((err) => {
  //         this._loading.hideLoading();
  //         const ReloadError = this._translateService.instant('Register.ReloadError') || {
  //           'Title': 'Refresh failed',
  //           'Message': 'The operation has been completed successfully but something went wrong during refresh.'
  //         };
  //         this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
  //           this._router.navigate(['/requests/dining']);
  //         });
  //       });
  //     }, (err) => {
  //       this._loading.hideLoading();
  //       this._errorService.showError(err, {
  //         continueLink: '.'
  //       });      
  //     }
  //   );               
  // }


  // rejectAttachment(attachment: any, templateToLoad: TemplateRef<any>) {
  //   this.attachmentToReject = attachment;
  //   this.modalRef = this._modalService.show(templateToLoad); 
  // }




  reset2(templateToLoad: TemplateRef<any>) {
    console.log(" Θελω να ακυρώσω την κάρτα")

    // const ResetConfirm = this._translateService.instant('UniversisDiningModule.ResetConfirm') || {
    //   Title: 'Change application status',
    //   // tslint:disable-next-line: max-line-length
    //   Message: 'You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?'
    // };
    // this._modal.showDialog(ResetConfirm.Title, ResetConfirm.Message, DIALOG_BUTTONS.YesNo, {
    //   theme: 'modal-dialog-danger'
    // }).then((result) => {
    //   if (result === 'yes') {        
    //     this._loading.showLoading();                        
    //     // set effective status of potential request
    //     const effectiveStatus = (this.model.attachments && this.model.attachments.some(x => x.published === false)) ? 'RejectedAttachmentsEffectiveStatus' : 'AcceptedAttachmentsEffectiveStatus';
    //     this._context.model('DiningRequestActions').save({
    //       id: this.model.id,
    //       actionStatus: {
    //         alternateName: 'PotentialActionStatus'
    //       },
    //       effectiveStatus: {
    //         alternateName: effectiveStatus
    //       }
    //     }).then(() => {
    //       // reload page
    //       return this.reload().then(() => {
    //         this._loading.hideLoading();
    //         // send an application event
    //         this._appEvent.change.next({
    //           model: 'DiningRequestActions',
    //           target: this.model
    //         });
    //       }).catch((err) => {
    //         this._loading.hideLoading();
    //         const ReloadError = this._translateService.instant('Register.ReloadError') || {
    //           'Title': 'Refresh failed',
    //           'Message': 'The operation has been completed successfully but something went wrong during refresh.'
    //         };
    //         this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
    //           this._router.navigate(['/candidates']);
    //         });
    //       });
    //     }).catch((err) => {
    //       this._loading.hideLoading();
    //       this._errorService.showError(err, {
    //         continueLink: '.'
    //       });
    //     });
    //   }
    // });

    // const RejectConfirm = this._translateService.instant('UniversisDiningModule.RejectConfirm') || {
    //   //     Title: 'Reject and cancel',
    //     Message: 'You are going to reject this application. Do you want to proceed?'
    //   };
    //   this._modal.showDialog(RejectConfirm.Title, RejectConfirm.Message, DIALOG_BUTTONS.YesNo, {
    //     theme: 'modal-dialog-danger'
    //   }).then((result) => {
    //     if (result === 'yes') {
    //       this._loading.showLoading();
    //       this._context.model('DiningRequestActions').save({
    //         id: this.model.id,
    //         actionStatus: {
    //           alternateName: 'CancelledActionStatus'
    //         }
    //       }).then(() => {
    //         // reload page
    //         return this.reload().then(() => {
    //           this._loading.hideLoading();
    //           // send an application event
    //           this._appEvent.change.next({
    //             model: 'DiningRequestActions',
    //             target: this.model
    //           });
    //         }).catch((err) => {
    //           this._loading.hideLoading();
    //           const ReloadError = this._translateService.instant('Register.ReloadError') || {
    //             'Title': 'Refresh failed',
    //             'Message': 'The operation has been completed successfully but something went wrong during refresh.'
    //           };
    //           this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
    //             this._router.navigate(['/candidates']);
    //           });
    //         });
    //       }).catch((err) => {
    //         this._loading.hideLoading();
    //         this._errorService.showError(err, {
    //           continueLink: '.'
    //         });
    //       });
    //     }
    //   });
    // }
  }

  // accept() {
  //   const AcceptConfirm = this._translateService.instant('UniversisDiningModule.AcceptConfirm') || {
  //     Title: 'Accept',
  //     Message: 'You are going to finally accept this application. Do you want to proceed?'
  //   };
  //   this._modal.showDialog(AcceptConfirm.Title, AcceptConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
  //     if (result === 'yes') {
  //       this._loading.showLoading();
  //       this._context.model('DiningRequestActions').save({
  //         id: this.model.id,
  //         actionStatus: {
  //           alternateName: 'CompletedActionStatus'
  //         }
  //       }).then(() => {
  //         // reload page
  //         return this.reload().then(() => {
  //           this._loading.hideLoading();
  //           // send an application event
  //           this._appEvent.change.next({
  //             model: 'DiningRequestActions',
  //             target: this.model
  //           });
  //         }).catch((err) => {
  //           this._loading.hideLoading();
  //           const ReloadError = this._translateService.instant('Register.ReloadError') || {
  //             'Title': 'Refresh failed',
  //             'Message': 'The operation has been completed successfully but something went wrong during refresh.'
  //           };
  //           this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
  //             this._router.navigate(['/candidates']);
  //           });
  //         });
  //       }).catch((err) => {
  //         this._loading.hideLoading();
  //         this._errorService.showError(err, {
  //           continueLink: '.'
  //         });
  //       });
  //     }
  //   });
  // }

  // reject() {
  //   const RejectConfirm = this._translateService.instant('UniversisDiningModule.RejectConfirm') || {
  //     Title: 'Reject and cancel',
  //     Message: 'You are going to reject this application. Do you want to proceed?'
  //   };
  //   this._modal.showDialog(RejectConfirm.Title, RejectConfirm.Message, DIALOG_BUTTONS.YesNo, {
  //     theme: 'modal-dialog-danger'
  //   }).then((result) => {
  //     if (result === 'yes') {
  //       this._loading.showLoading();
  //       this._context.model('DiningRequestActions').save({
  //         id: this.model.id,
  //         actionStatus: {
  //           alternateName: 'CancelledActionStatus'
  //         }
  //       }).then(() => {
  //         // reload page
  //         return this.reload().then(() => {
  //           this._loading.hideLoading();
  //           // send an application event
  //           this._appEvent.change.next({
  //             model: 'DiningRequestActions',
  //             target: this.model
  //           });
  //         }).catch((err) => {
  //           this._loading.hideLoading();
  //           const ReloadError = this._translateService.instant('Register.ReloadError') || {
  //             'Title': 'Refresh failed',
  //             'Message': 'The operation has been completed successfully but something went wrong during refresh.'
  //           };
  //           this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
  //             this._router.navigate(['/candidates']);
  //           });
  //         });
  //       }).catch((err) => {
  //         this._loading.hideLoading();
  //         this._errorService.showError(err, {
  //           continueLink: '.'
  //         });
  //       });
  //     }
  //   });
  // }

  // revert() {
  //   const RevertConfirm = this._translateService.instant('UniversisDiningModule.RevertConfirm') || {
  //     Title: 'Activate application',
  //     Message: 'You are going to activate this application. Do you want to proceed?'
  //   };
  //   this._modal.showDialog(RevertConfirm.Title, RevertConfirm.Message, DIALOG_BUTTONS.YesNo, {
  //     theme: 'modal-dialog-danger'
  //   }).then((result) => {
  //     if (result === 'yes') {
  //       this._loading.showLoading();
  //       this._context.model('DiningRequestActions').save({
  //         id: this.model.id,
  //         actionStatus: {
  //           alternateName: 'ActiveActionStatus'
  //         }
  //       }).then(() => {
  //         // reload page
  //         return this.reload().then(() => {
  //           this._loading.hideLoading();
  //           // send an application event
  //           this._appEvent.change.next({
  //             model: 'DiningRequestActions',
  //             target: this.model
  //           });
  //         }).catch((err) => {
  //           this._loading.hideLoading();
  //           const ReloadError = this._translateService.instant('Register.ReloadError') || {
  //             'Title': 'Refresh failed',
  //             'Message': 'The operation has been completed successfully but something went wrong during refresh.'
  //           };
  //           this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
  //             this._router.navigate(['/candidates']);
  //           });
  //         });
  //       }).catch((err) => {
  //         this._loading.hideLoading();
  //         this._errorService.showError(err, {
  //           continueLink: '.'
  //         });
  //       });
  //     }
  //   });
  // }

  // reset() {
  //   const ResetConfirm = this._translateService.instant('UniversisDiningModule.ResetConfirm') || {
  //     Title: 'Change application status',
  //     // tslint:disable-next-line: max-line-length
  //     Message: 'You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?'
  //   };
  //   this._modal.showDialog(ResetConfirm.Title, ResetConfirm.Message, DIALOG_BUTTONS.YesNo, {
  //     theme: 'modal-dialog-danger'
  //   }).then((result) => {
  //     if (result === 'yes') {        
  //       this._loading.showLoading();                        
  //       // set effective status of potential request
  //       const effectiveStatus = (this.model.attachments && this.model.attachments.some(x => x.published === false)) ? 'RejectedAttachmentsEffectiveStatus' : 'AcceptedAttachmentsEffectiveStatus';
  //       this._context.model('DiningRequestActions').save({
  //         id: this.model.id,
  //         actionStatus: {
  //           alternateName: 'PotentialActionStatus'
  //         },
  //         effectiveStatus: {
  //           alternateName: effectiveStatus
  //         }
  //       }).then(() => {
  //         // reload page
  //         return this.reload().then(() => {
  //           this._loading.hideLoading();
  //           // send an application event
  //           this._appEvent.change.next({
  //             model: 'DiningRequestActions',
  //             target: this.model
  //           });
  //         }).catch((err) => {
  //           this._loading.hideLoading();
  //           const ReloadError = this._translateService.instant('Register.ReloadError') || {
  //             'Title': 'Refresh failed',
  //             'Message': 'The operation has been completed successfully but something went wrong during refresh.'
  //           };
  //           this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
  //             this._router.navigate(['/candidates']);
  //           });
  //         });
  //       }).catch((err) => {
  //         this._loading.hideLoading();
  //         this._errorService.showError(err, {
  //           continueLink: '.'
  //         });
  //       });
  //     }
  //   });
  // }

  // onEditReviewEvent(event) {
  //   if (event.data && event.data.cancel === true) {
  //     this.editingReview = false;
  //   } else if (event.data && event.data.addReview === true) {
  //     // add or update review
  //     this._context.model(`CourseClassRegisterActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
  //       this.editingReview = false;
  //       // update data
  //       const findItem = this.model.courseRegistrations.find((item) => {
  //         return item.id === result.itemReviewed;
  //       });
  //       if (findItem) {
  //         if (findItem.review) {
  //           Object.assign(findItem.review, result);
  //         } else {
  //           findItem.review = result;
  //         }
  //         // send an application event
  //         this._appEvent.change.next({
  //           model: 'CourseClassRegisterActions',
  //           target: findItem
  //         });
  //       }

  //     }).catch((err) => {
  //       this._errorService.showError(err, {
  //         continueLink: '.'
  //       });
  //     });

  //   }
  // }

  // onItemReviewEvent(event) {
  //   if (event.data && event.data.cancel === true) {
  //     this.editingReview = false;
  //   } else if (event.data && event.data.addReview === true) {
  //     // add or update review
  //     this._context.model(`DiningRequestActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
  //       return this._context.model(`DiningRequestActions/${event.data.itemReviewed}/review`)
  //         .asQueryable().expand('createdBy', 'modifiedBy').getItem().then((finalResult) => {
  //           this.editingReview = false;
  //           this.model.review = finalResult;
  //         });
  //     }).catch((err) => {
  //       this._errorService.showError(err, {
  //         continueLink: '.'
  //       });
  //     });

  //   }
  // }

  // onMessageEvent(event) {
  //   if (event.data && event.data.cancelMessage) {
  //     this.showNewMessage = false;
  //     this.newMessage = {};
  //   }
  // }

  // async sendWithoutAttachment(message) {
  //   try {
  //     this._loading.showLoading();
  //     // set recipient (which is this action owner)
  //     Object.assign(message, {
  //       student: this.model.student,
  //       recipient: this.model.owner
  //     });
  //     await this._context.model(`DiningRequestActions/${this.model.id}/messages`).save(message);
  //     // reload message
  //     this.messages = await this._context.model(`DiningRequestActions/${this.model.id}/messages`)
  //       .asQueryable()
  //       .orderBy('dateCreated desc')
  //       .expand('attachments')
  //       .getItems();
  //     // clear message
  //     this.newMessage = {
  //       attachments: []
  //     };
  //     this.showNewMessage = false;
  //     this._loading.hideLoading();
  //   } catch (err) {
  //     this._loading.hideLoading();
  //     this._errorService.showError(err, {
  //       continueLink: '.'
  //     });
  //   }
  // }

  // async send(message) {
  //   try {
  //     if (!(message.attachments && message.attachments.length)) {
  //       // send message without attachment
  //       await this.sendWithoutAttachment(message);
  //       return;
  //     }
  //     this._loading.showLoading();

  //     // set recipient (which is this action owner)
  //     Object.assign(message, {
  //       recipient: this.model.owner
  //     });

  //     const formData: FormData = new FormData();
  //     // get attachment if any
  //     if (message.attachments && message.attachments.length) {
  //       formData.append('attachment', message.attachments[0], message.attachments[0].name);
  //     }
  //     Object.keys(message).filter((key) => {
  //       return key !== 'attachments';
  //     }).forEach((key) => {
  //       if (Object.prototype.hasOwnProperty.call(message, key)) {
  //         formData.append(key, message[key]);
  //       }
  //     });
  //     // get context service headers
  //     const serviceHeaders = this._context.getService().getHeaders();
  //     const serviceUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/sendMessage`);
  //     await this._http.post(serviceUrl, formData, {
  //       headers: serviceHeaders
  //     }).toPromise();
  //     // reload message
  //     this.messages = await this._context.model(`DiningRequestActions/${this.model.id}/messages`)
  //       .asQueryable()
  //       .orderBy('dateCreated desc')
  //       .expand('attachments')
  //       .getItems();
  //     // clear message
  //     // clear message
  //     this.newMessage = {
  //       attachments: []
  //     };
  //     this.showNewMessage = false;
  //     this._loading.hideLoading();
  //   } catch (err) {
  //     this._loading.hideLoading();
  //     this._errorService.showError(err, {
  //       continueLink: '.'
  //     });
  //   }

  // }



}
