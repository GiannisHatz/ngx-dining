import { Component, OnInit, OnDestroy, Input, ViewEncapsulation, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ModalService, DIALOG_BUTTONS, LoadingService, ErrorService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { DataServiceQueryParams, ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';
import { AppEventService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
// import { Subscription } from 'rxjs';
@Component({
  selector: 'dining-edit-action',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditComponent implements OnInit, OnDestroy {

  public static readonly ServiceQueryParams = {
    $expand: 'messages,student($expand=person),attachments($expand=attachmentType)'
  };

  dataSubscription: any;
  paramSubscription: any;
  editingReview: any = false;
  @ViewChild('form') form: AdvancedFormComponent;
  @Input() model: any;
  @Input() showNavigation = true;
  @Input() showActions = true;
  public modalRef: BsModalRef;
  public attachmentToReject: any;  
  public messages: any[];
  public otherApplications: any[];
  newMessage: any = {
    attachments: []
  };
  public showNewMessage = false;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _modal: ModalService,
    private _modalService: BsModalService,    
    private _loading: LoadingService,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _http: HttpClient,
    private _appEvent: AppEventService) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((query) => {
      //
    });
    this.dataSubscription = this._activatedRoute.data.subscribe((data) => {
      this.model = data.model;
      if (this.form) {
        this.form.refreshForm.emit({
          submission: {
            data: this.model
          }
        });
      }
      // get messages
      if (this.model) {

        // get other active applications of the same user
        this._context.model('DiningRequestActions')
          .where('owner').equal(this.model.owner)
          .and('actionStatus/alternateName').equal('ActiveActionStatus')
          .orderByDescending('dateModified')
          .getItems().then((results) => {
            this.otherApplications = results.filter((item) => {
              return item.id !== this.model.id;
            });
          });

        this._context.model(`DiningRequestActions/${this.model.id}/messages`)
          .asQueryable()
          .orderBy('dateCreated desc')
          .expand('attachments')
          .getItems().then((results) => {
            this.messages = results;
          }).catch((err) => {
            console.error(err);
          });
      }
    });
  }

  reload() {
    if (this.model == null) {
      // do nothing
      return;
    }
    return this._context.model('DiningRequestActions')
      .asQueryable(<DataServiceQueryParams>EditComponent.ServiceQueryParams)
      .where('id').equal(this.model.id)
      .getItem().then((result) => {
        this.model = result;
      });
  }

  preview(attachment: any) {
    return this._router.navigate(['.'], {
      replaceUrl: false,
      relativeTo: this._activatedRoute,
      skipLocationChange: true,
      queryParams: {
        download: attachment.url
      }
    });
  }

  download(attachment: any) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  rejectAttachment(attachment: any, templateToLoad: TemplateRef<any>) {
    this.attachmentToReject = attachment;
    this.modalRef = this._modalService.show(templateToLoad); 
  }

  // confirm callback of the reject attachment modal 
  // user accepted rejection => reject attachment and close modal
  public confirmRejectAttachmentModal( rejectionName: string ) {
    if (rejectionName && this.attachmentToReject !== null) {
      this.modalRef.hide();
      const headers = new Headers();
      const serviceHeaders = this._context.getService().getHeaders();
      // reject attachment
      const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/rejectAttachment`);
      this._loading.showLoading();
      return this._http.post(postUrl, this.attachmentToReject, {
        headers: serviceHeaders
      }).subscribe(async (result) => {
          if (result) {
            // notify user about reason of rejection by message                 
            const message = {
              subject: this._translateService.instant('UniversisDiningModule.RejectAttachmentSubject'),
              body: this._translateService.instant('UniversisDiningModule.RejectAttachmentBody', { attachmentName: this.attachmentToReject.attachmentType.name, rejectionName: rejectionName })
            };
            await this.sendWithoutAttachment(message);
            // clear 
            this.attachmentToReject = null;            
          }          
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/requests/dining']);
            });
          });
        }, (err) => {
          this._loading.hideLoading();
          // clear
          this.attachmentToReject = null;
          this._errorService.showError(err, {
            continueLink: '.'
          });      
        });               
    } else {
      // User did not insert any text
      alert(this._translateService.instant('UniversisDiningModule.RejectAttachmentModal.TitleOnUserError'));
    }
  }
  
  // cancel callback of the reject attachment modal 
  // user declined rejection => close modal
  public closeRejectAttachmentModal(): void {
    this.modalRef.hide();
  }

  revertAttachment(attachment: any) {
    // revert (accept back) attachment
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();    
    const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/revertAttachment`);
    this._loading.showLoading();
    return this._http.post(postUrl, attachment, {
      headers: serviceHeaders
    }).subscribe(async (result) => {
        // reload page
        return this.reload().then(() => {
          this._loading.hideLoading();
          // send an application event
          this._appEvent.change.next({
            model: 'DiningRequestActions',
            target: this.model
          });
        }).catch((err) => {
          this._loading.hideLoading();
          const ReloadError = this._translateService.instant('Register.ReloadError') || {
            'Title': 'Refresh failed',
            'Message': 'The operation has been completed successfully but something went wrong during refresh.'
          };
          this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
            this._router.navigate(['/requests/dining']);
          });
        });
      }, (err) => {
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });      
      }
    );               
  }

  accept() {
    const AcceptConfirm = this._translateService.instant('UniversisDiningModule.AcceptConfirm') || {
      Title: 'Accept',
      Message: 'You are going to finally accept this application. Do you want to proceed?'
    };
    this._modal.showDialog(AcceptConfirm.Title, AcceptConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'CompletedActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  reject() {
    const RejectConfirm = this._translateService.instant('UniversisDiningModule.RejectConfirm') || {
      Title: 'Reject and cancel',
      Message: 'You are going to reject this application. Do you want to proceed?'
    };
    this._modal.showDialog(RejectConfirm.Title, RejectConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'CancelledActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  revert() {
    const RevertConfirm = this._translateService.instant('UniversisDiningModule.RevertConfirm') || {
      Title: 'Activate application',
      Message: 'You are going to activate this application. Do you want to proceed?'
    };
    this._modal.showDialog(RevertConfirm.Title, RevertConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'ActiveActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  reset() {
    const ResetConfirm = this._translateService.instant('UniversisDiningModule.ResetConfirm') || {
      Title: 'Change application status',
      // tslint:disable-next-line: max-line-length
      Message: 'You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?'
    };
    this._modal.showDialog(ResetConfirm.Title, ResetConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {        
        this._loading.showLoading();                        
        // set effective status of potential request
        const effectiveStatus = (this.model.attachments && this.model.attachments.some(x => x.published === false)) ? 'RejectedAttachmentsEffectiveStatus' : 'AcceptedAttachmentsEffectiveStatus';
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'PotentialActionStatus'
          },
          effectiveStatus: {
            alternateName: effectiveStatus
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  onEditReviewEvent(event) {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context.model(`CourseClassRegisterActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
        this.editingReview = false;
        // update data
        const findItem = this.model.courseRegistrations.find((item) => {
          return item.id === result.itemReviewed;
        });
        if (findItem) {
          if (findItem.review) {
            Object.assign(findItem.review, result);
          } else {
            findItem.review = result;
          }
          // send an application event
          this._appEvent.change.next({
            model: 'CourseClassRegisterActions',
            target: findItem
          });
        }

      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });

    }
  }

  onItemReviewEvent(event) {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context.model(`DiningRequestActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
        return this._context.model(`DiningRequestActions/${event.data.itemReviewed}/review`)
          .asQueryable().expand('createdBy', 'modifiedBy').getItem().then((finalResult) => {
            this.editingReview = false;
            this.model.review = finalResult;
          });
      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });

    }
  }

  onMessageEvent(event) {
    if (event.data && event.data.cancelMessage) {
      this.showNewMessage = false;
      this.newMessage = {};
    }
  }

  async sendWithoutAttachment(message) {
    try {
      this._loading.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        student: this.model.student,
        recipient: this.model.owner
      });
      await this._context.model(`DiningRequestActions/${this.model.id}/messages`).save(message);
      // reload message
      this.messages = await this._context.model(`DiningRequestActions/${this.model.id}/messages`)
        .asQueryable()
        .orderBy('dateCreated desc')
        .expand('attachments')
        .getItems();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async send(message) {
    try {
      if (!(message.attachments && message.attachments.length)) {
        // send message without attachment
        await this.sendWithoutAttachment(message);
        return;
      }
      this._loading.showLoading();

      // set recipient (which is this action owner)
      Object.assign(message, {
        student: this.model.student.id,
        recipient: this.model.owner
      });

      const formData: FormData = new FormData();
      // get attachment if any
      if (message.attachments && message.attachments.length) {
        formData.append('attachment', message.attachments[0], message.attachments[0].name);
      }
      Object.keys(message).filter((key) => {
        return key !== 'attachments';
      }).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(message, key)) {
          formData.append(key, message[key]);
        }
      });
      // get context service headers
      const serviceHeaders = this._context.getService().getHeaders();
      const serviceUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/sendMessage`);
      await this._http.post(serviceUrl, formData, {
        headers: serviceHeaders
      }).toPromise();
      // reload message
      this.messages = await this._context.model(`DiningRequestActions/${this.model.id}/messages`)
        .asQueryable()
        .orderBy('dateCreated desc')
        .expand('attachments')
        .getItems();
      // clear message
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }

  }

  downloadFile(attachment) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loading.showLoading();
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status != 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loading.hideLoading();
      }).catch((err) => {
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }

}
