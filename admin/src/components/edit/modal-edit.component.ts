import { Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { RouterModalOkCancel, RouterModal } from '@universis/common/routing';
import { ActivatedRoute, Router } from '@angular/router';
import { AppEventService } from '@universis/common';
import { ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';
import { AdvancedFormComponent } from '@universis/forms';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'dining-modal-edit-action',
  templateUrl: './modal-edit.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ModalEditComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  dataSubscription: Subscription;
  public showActions = false;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _translateService: TranslateService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'mx-w-90';
    this.modalTitle = this._translateService.instant('Settings.EditItem') + ' ('
      + this._translateService.instant('NewRequestTemplates.DiningRequestAction.Name') + ')';
    this.dataSubscription = this.activatedRoute.data.subscribe((data) => {
      if (data.action === 'modal-edit') {
        this.showActions = true;
      }
    });
  }

  ngOnInit() {
    this.cancelButtonClass = 'd-none';
    this.okButtonText = this._translateService.instant('Reports.Viewer.Close');
  }

  cancel(): Promise<any> {
    return super.close();
  }

  ok(): Promise<any> {
    return super.close();
  }

}

